//
//	  UnityOSC - Example of usage for OSC receiver
//
//	  Copyright (c) 2012 Jorge Garcia Martin
//
// 	  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// 	  documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// 	  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
// 	  and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// 	  The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// 	  of the Software.
//
// 	  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// 	  TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// 	  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// 	  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// 	  IN THE SOFTWARE.
//	  GIT https://github.com/jorgegarcia/UnityOSC

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityOSC;

public class OSCControl : MonoBehaviour {

	public int listenerPort = 8000;
	public string transmitterIP = "127.0.0.1";
	public int transmitterPort = 8001;

	private Dictionary<string, ServerLog> servers;
	private Dictionary<string, ClientLog> clients;
	
//	public Transform meandPrefab; 
//	public Vector2 meand;
	
	public GameObject euglenaPrefab;
	
	public float euglenaPosScale = 0.1f;
	public Vector3 euglenaPosOffset; 

	public bool active = false; 

	void Awake () {
		DontDestroyOnLoad(transform.gameObject);
	}

	public void SetListenerPort(int port) {
		listenerPort =  port;
	}

	public void SetTransmitterIP(string ip) {
		transmitterIP = ip;
	}

	public void SetTransmitterPort(int port) {
		transmitterPort = port;
	}

	public void ActivateOSC() {
			if(!active) {
			OSCHandler.Instance.Init (listenerPort, transmitterIP, transmitterPort); //init OSC
				servers = new Dictionary<string, ServerLog> ();
				clients = new Dictionary<string,ClientLog> ();
				active = true;
			}
	}

	public void DeactivateOSC() {
			OSCHandler.Instance.OnApplicationQuit ();
			active = false;
	}

	void Update() {
		if (active) {

			OSCHandler.Instance.UpdateLogs ();

			servers = OSCHandler.Instance.Servers;
			clients = OSCHandler.Instance.Clients;

			foreach (KeyValuePair<string, ServerLog> item in servers) {

				if (item.Value.log.Count > 0) {
					int lastPacketIndex = item.Value.packets.Count - 1;
						
					// 				UnityEngine.Debug.Log (String.Format ("SERVER: {0} ADDRESS: {1} VALUE : {2}", 
					// 					                                    item.Key, // Server name
					// 					                                    item.Value.packets [lastPacketIndex].Address, // OSC address
					// 					                                    item.Value.packets [lastPacketIndex].Data [0].ToString ())); //First data value
						
//					if (item.Value.packets [lastPacketIndex].Address == "/meand") {
//						//converts the values
//						meand.x = float.Parse (item.Value.packets [lastPacketIndex].Data [1].ToString ());
//						meand.y = float.Parse (item.Value.packets [lastPacketIndex].Data [0].ToString ());
//						Debug.Log ("X:"+meand.x + "Y:"+meand.y);
//						if (meandPrefab) {
//							Vector3 lookAtPoint = new Vector3 (meand.x, meandPrefab.position.y, meand.y);
//							meandPrefab.LookAt (lookAtPoint);
//						}
//					}
				
					if (item.Value.packets [lastPacketIndex].Address == "/euglena") {

						int id = int.Parse (item.Value.packets [lastPacketIndex].Data [0].ToString ());
						float x = float.Parse (item.Value.packets [lastPacketIndex].Data [1].ToString ());
						float y = float.Parse (item.Value.packets [lastPacketIndex].Data [2].ToString ());
						float w = float.Parse (item.Value.packets [lastPacketIndex].Data [3].ToString ());
						float h = float.Parse (item.Value.packets [lastPacketIndex].Data [4].ToString ());
						float dx = float.Parse (item.Value.packets [lastPacketIndex].Data [5].ToString ());
						float dy = float.Parse (item.Value.packets [lastPacketIndex].Data [6].ToString ());

						Transform existingChild = null;
						Vector3 newPos = new Vector3 (x * euglenaPosScale + euglenaPosOffset.x,
						                             	euglenaPosOffset.y,
						                             	y * euglenaPosScale * -1 + euglenaPosOffset.z); 
						//-1 flip Bildschirmkoordinaten

						foreach (Transform child in transform) {
							if (child.name == "Euglena(Clone)") {
								if (child.GetComponent<EuglenaObject> ().id == id)
									existingChild = child;
							}
						}

						if (!existingChild) {

							Quaternion newRot = Quaternion.identity;
							newRot.eulerAngles = new Vector3 (dx, 0f, dy);

							GameObject clone = (GameObject)Instantiate (euglenaPrefab, newPos, newRot);
							clone.GetComponent<EuglenaObject> ().id = id;
							clone.transform.parent = transform;
							clone.transform.localScale = new Vector3 (w, 1f, h);

						} else {
							existingChild.GetComponent<EuglenaObject> ().SetNewPosition (newPos);
						}
					}

				
				}			
			} 
		}
	}

	public void SendOSCMsg( String address, float value) {

		OSCHandler.Instance.SendMessageToClient ("Processing", address, value);
		OSCHandler.Instance.UpdateLogs();
	
	}
}
﻿using UnityEngine;
using System.Collections;

public class CubeSpawner : MonoBehaviour {

	public Transform cube;
	public Vector3 distBetweenCubes;
	public Vector3 offsetAll;
	public int cubesPerRowX;
	public int cubesPerRowY;
	public int cubesPerRowZ;

	// Use this for initialization
	void Start () {


		for (int r = 0; r < cubesPerRowX; r++) {

			for (int i = 0; i < cubesPerRowY; i++) {
	
				for (int j = 0; j < cubesPerRowZ; j++) {

					Vector3 newPos = new Vector3 ( r*distBetweenCubes.x, i*distBetweenCubes.y, j*distBetweenCubes.z);
					newPos.x += offsetAll.x;
					newPos.y += offsetAll.y;
					newPos.z += offsetAll.z;
					Instantiate (cube, newPos, transform.rotation);

				}

			}

		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class TriggerLED : MonoBehaviour {

	public bool active;
	public string ledMsgAddress = "/LED/1";
	private OSCControl osc;

	// Use this for initialization
	void Awake () {
		osc = GameObject.Find("OSC").GetComponent<OSCControl>();

		GetComponent<Renderer>().material.EnableKeyword ("_EMISSION");
		GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(0.0f, 0.0f, 0.0f, 0.0f));

		if (active)
			ActivateLed ();
		else
			DeactivateLed ();
	}

	void OnMouseOver() {
		if (Input.GetMouseButtonDown (0)) {
			if (!active)
				ActivateLed ();
			else
				DeactivateLed ();
		}
	}

	public void ActivateLed () {
		GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(1.0f, 1.0f, 1.0f, 1.0f));
		active = true;
		foreach (Transform child in transform) {
			child.gameObject.SetActive(active);
		}
		osc.SendOSCMsg(ledMsgAddress, active.GetHashCode() );
		Debug.Log ("Activate LED");
	}

	public void DeactivateLed () {
		GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(0.0f, 0.0f, 0.0f, 0.0f));
		active = false;
		foreach (Transform child in transform) {
			child.gameObject.SetActive(active);
		}
		osc.SendOSCMsg (ledMsgAddress, active.GetHashCode ());
		Debug.Log ("Deactivate LED");
	}
	
}

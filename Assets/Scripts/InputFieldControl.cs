﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InputFieldControl : MonoBehaviour {

	public string sendTo = "SetListenerPort";

	private OSCControl osc;

	void Start () {
		osc = GameObject.Find ("OSC").GetComponent<OSCControl> ();
		InputField input = gameObject.GetComponent<InputField>();
		input.onEndEdit.AddListener(SubmitName);

		//TODO: set inputfield data from OSCCOntrol
	}
	
	private void SubmitName(string arg)
	{
		if (sendTo == "SetListenerPort") {
			osc.SetListenerPort(int.Parse (arg));
		}

		if (sendTo == "SetTransmitterIP") {
			osc.SetTransmitterIP(arg);
		}

		if (sendTo == "SetTransmitterPort") {
			osc.SetTransmitterPort(int.Parse (arg));
		}
	}
}

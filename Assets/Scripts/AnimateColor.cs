using UnityEngine;
using System.Collections;

public class AnimateColor : MonoBehaviour {
	
	public Color[] colors;	
  	private int currentIndex = 0;
	private int nextIndex;
	public float changeColourTime = 0.1f;
	private float timer = 0.0f;

	void Start() {
		if (colors == null || colors.Length < 2) {
			colors = new Color[6];
			colors[0] = Color.blue;
			colors[1] = Color.cyan;
			colors[2] = Color.green;
			colors[3] = Color.yellow;
			colors[4] = Color.red;
			colors[5] = Color.magenta;

		}

		nextIndex = (currentIndex + 1) % colors.Length;    

	}
	
	void Update() {

		timer += Time.deltaTime;
			
		if (timer > changeColourTime) {
			
			currentIndex = (currentIndex + 1) % colors.Length;
			nextIndex = (currentIndex + 1) % colors.Length;
			timer = 0.0f;
				
		}
		GetComponent<Renderer>().material.color = Color.Lerp (colors [currentIndex], colors [nextIndex], timer / changeColourTime);

	}

}
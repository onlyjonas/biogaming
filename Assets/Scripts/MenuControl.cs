﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuControl : MonoBehaviour {

	public Toggle oscToggle;
	public Toggle camToggle;
	public Toggle ledToggle1;
	public Toggle ledToggle2;
	public Toggle ledToggle3;
	public Toggle ledToggle4;

	public GameObject topCam;
	public GameObject fpsCam;

	public string ledToggleAddress = "/LED";
	private string[] ledToggleKeys;
	public GameObject led1;
	public GameObject led2;
	public GameObject led3;
	public GameObject led4;

	private OSCControl osc;
	private bool isShowing = true;

	void Start() {
		osc = GameObject.Find("OSC").GetComponent<OSCControl>();

		ledToggle1.isOn = led1.transform.GetComponent<TriggerLED> ().active;
		ledToggle2.isOn = led2.transform.GetComponent<TriggerLED> ().active;
		ledToggle3.isOn = led3.transform.GetComponent<TriggerLED> ().active;
		ledToggle4.isOn = led4.transform.GetComponent<TriggerLED> ().active;
	}

	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyUp ("o")) {
			if (osc.active) {
				osc.DeactivateOSC ();
				oscToggle.isOn = false;
			} else {
				osc.ActivateOSC ();
				oscToggle.isOn = true;
			}
		}

		if (Input.GetKeyUp("c")) {
			if(topCam.activeSelf) {
				camToggle.isOn = true;
			} else {
				camToggle.isOn = false;
			}
			ToggleCam();
		}

		if (Input.GetKeyUp("1")) {
			if(led1.activeSelf) {
				ledToggle1.isOn = false;
			} else {
				ledToggle1.isOn = true;
			}
			ToggleLed1();
		}

		if (Input.GetKeyUp("2")) {
			if(led2.activeSelf) {
				ledToggle2.isOn = false;
			} else {
				ledToggle2.isOn = true;
			}
			ToggleLed2();
		}

		if (Input.GetKeyUp("3")) {
			if(led3.activeSelf) {
				ledToggle3.isOn = false;
			} else {
				ledToggle3.isOn = true;
			}
			ToggleLed3();
		}

		if (Input.GetKeyUp("4")) {
			if(led4.activeSelf) {
				ledToggle4.isOn = false;
			} else {
				ledToggle4.isOn = true;
			}
			ToggleLed4();
		}

		if (Input.GetKeyUp (KeyCode.Tab)) {
			Canvas canvas = this.GetComponent<Canvas>();
			isShowing = !isShowing;
			canvas.enabled = isShowing;
		}


	}


	public void ToggleOSC(){
		if(oscToggle.isOn) osc.ActivateOSC();
		else osc.DeactivateOSC();
	}

	public void ToggleCam(){
		if(camToggle.isOn) {
			topCam.SetActive(false);
			fpsCam.SetActive(true);
		} else {
			topCam.SetActive(true);
			fpsCam.SetActive(false);
		}
	}

	public void ToggleLed1(){
		if (ledToggle1.isOn) led1.transform.GetComponent<TriggerLED> ().ActivateLed();
		else led1.transform.GetComponent<TriggerLED> ().DeactivateLed();
	}

	public void ToggleLed2(){
		if (ledToggle2.isOn) led2.transform.GetComponent<TriggerLED> ().ActivateLed();
		else led2.transform.GetComponent<TriggerLED> ().DeactivateLed();
	}

	public void ToggleLed3(){
		if (ledToggle3.isOn) led3.transform.GetComponent<TriggerLED> ().ActivateLed();
		else led3.transform.GetComponent<TriggerLED> ().DeactivateLed();
	}

	public void ToggleLed4(){
		if (ledToggle4.isOn) led4.transform.GetComponent<TriggerLED> ().ActivateLed();
		else led4.transform.GetComponent<TriggerLED> ().DeactivateLed();
	}

	public void ButtonEndGame() {
		osc.DeactivateOSC ();
		DestroyImmediate (osc);
		Application.LoadLevel("StartGame");
	}

}

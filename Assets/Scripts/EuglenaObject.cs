﻿using UnityEngine;
using System.Collections;

public class EuglenaObject : MonoBehaviour {

	public int id;
	public Vector3 pos;
	public float idleLifeTime = 1.0f;
	public float speed = 1.0f;
	public Transform keyPrefab;
	public AudioClip collisionSound;
	private float timeLeft;

	
	// Use this for initialization
	void Start () {
	  pos = transform.position;
	  timeLeft = idleLifeTime;
	}
	
	// Update is called once per frame
	void Update () {
	
	    MoveToNewPos();
	    IdleLifeTimeCheck ();
	    
		Vector3 newScale = new Vector3 (timeLeft,timeLeft,timeLeft);
		float step = speed * Time.deltaTime;
		transform.localScale = Vector3.MoveTowards (transform.localScale, newScale, step);
	
	}

	public void SetNewPosition (Vector3 newPos)  {
	    pos = newPos;
	    timeLeft = idleLifeTime;
	}
	
	void MoveToNewPos () {
	    float step = speed * Time.deltaTime;
	    transform.position = Vector3.MoveTowards(transform.position, pos, step);
        }
	
	void IdleLifeTimeCheck () {
	
	    timeLeft -= Time.deltaTime;
	    if (timeLeft < 0) {
			SpawnKey();
			Destroy (gameObject);
		}
	}

	void SpawnKey () {
		Instantiate (keyPrefab, transform.position, Quaternion.identity);
	}

	void OnCollisionEnter(Collision collision)
	{
		if( collision.gameObject.tag == "Player" )
		{
			AudioSource.PlayClipAtPoint (collisionSound, transform.position);
			Debug.Log("COLLISION WITH PLAYER");
		}
	}

}

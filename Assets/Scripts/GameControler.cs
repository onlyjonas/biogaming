﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameControler : MonoBehaviour {
	
	public int keys = 0;
	public int maxKeys = 5;
	public Text keyText;
	public Button enterGameButton;
	public GameObject topCam;
	public GameObject fpsCam;
	public Transform levelGate;
	private OSCControl osc;

	void Start () {
		keys = 0;
		keyText.gameObject.SetActive(false);
		osc = GameObject.Find ("OSC").GetComponent<OSCControl> ();
		osc.ActivateOSC ();
		Cursor.visible = true;
	}

	public void AddKey () {
		keys++;
		keyText.text = "Keys: " + keys + " / "+ maxKeys;
		if (keys >= maxKeys)
			openNextLevelGate (); 
	}

	public void EnterGameSpace() {
		topCam.SetActive(false);
		fpsCam.SetActive(true);
		enterGameButton.gameObject.SetActive(false);
		keyText.gameObject.SetActive(true);
		Cursor.visible = false;
	}

	void openNextLevelGate () {
		Instantiate (levelGate, new Vector3 (0f, 7.5f, 0f), Quaternion.identity);
	}
}

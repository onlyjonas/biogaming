﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour {

	private OSCControl osc;

	public void LoadLevel () {
		Debug.Log ("START GAME");
		osc = GameObject.Find ("OSC").GetComponent<OSCControl> ();
		osc.DeactivateOSC ();
		Application.LoadLevel("Seek");	
	}
}

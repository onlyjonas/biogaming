﻿using UnityEngine;

public class LevelGate : MonoBehaviour {

	private OSCControl osc;

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			Debug.Log ("END OF LEVEL");
			osc = GameObject.Find ("OSC").GetComponent<OSCControl> ();
			osc.DeactivateOSC ();
			Application.LoadLevel("Seek");

		} else {
			Destroy (other.gameObject);
		}
	}
}

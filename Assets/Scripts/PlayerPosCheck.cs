﻿using UnityEngine;
using System.Collections;

public class PlayerPosCheck : MonoBehaviour {

	public float maxFallingDist = -10f;
	private Vector3 defaultPos;

	// Use this for initialization
	void Start () {
		defaultPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y < maxFallingDist)
			transform.position = defaultPos;
	}
}

﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour {

	public Transform cube;
	public Vector3 distBetweenCubes;
	public Vector3 offsetAll;

	void Awake () {
	
		/* LEVEL EDITOR - - - - */
		/* 0 = Empty (no cubes) 
		/* 1-9 = stacked cubes
		/* - - - - - - - - - -  */

		float r = Random.value;
		int[,] level;

		level = new int[,] { 
			{3,2,3,2,3,2,3,2,3,2},
			{2,0,0,0,0,0,0,0,0,3},
			{3,0,0,0,0,0,0,0,0,2},
			{2,0,0,1,1,1,1,0,0,3},
			{3,0,0,1,5,4,1,0,0,2},
			{2,0,0,1,2,3,1,0,0,3},
			{3,0,0,1,1,1,1,0,0,2},
			{2,0,0,0,0,0,0,0,0,3},
			{3,0,0,0,0,0,0,0,0,2},
			{2,3,2,3,2,3,2,3,2,3}
		};
	

		if (r > 0.3 && r < 0.6) {
			level = new int[,] { 
				{3,0,3,0,3,0,3,0,3,0},
				{0,3,0,3,0,3,0,3,0,3},
				{3,0,3,0,3,0,3,0,3,0},
				{0,3,0,3,0,3,0,3,0,3},
				{3,0,3,0,3,0,3,0,3,0},
				{0,3,0,3,0,3,0,3,0,3},
				{3,0,3,0,3,0,3,0,3,0},
				{0,3,0,3,0,3,0,3,0,3},
				{3,0,3,0,3,0,3,0,3,0},
				{0,3,0,3,0,3,0,3,0,3}
			};
		}

		if (r > 0.6) {
			level = new int[,] { 
				{3,3,3,3,3,3,3,3,3,3},
				{3,3,3,3,3,3,3,3,3,3},
				{3,3,3,3,3,3,3,3,3,3},
				{3,3,3,3,3,3,3,3,3,3},
				{3,3,3,3,3,3,3,3,3,3},
				{3,3,3,3,3,3,3,3,3,3},
				{3,3,3,3,3,3,3,3,3,3},
				{3,3,3,3,3,3,3,3,3,3},
				{3,3,3,3,3,3,3,3,3,3},
				{3,3,3,3,3,3,3,3,3,3}
			};
		}

		for (int i=0; i<level.GetLength(0); i++) {
			for (int j=0; j<level.GetLength(1); j++) {

				for (int k = 0; k < level[i,j] ; k++) {
						Vector3 newPos = new Vector3 ( i*distBetweenCubes.x, k*distBetweenCubes.y, j*distBetweenCubes.z);
						newPos.x += offsetAll.x;
						newPos.y += offsetAll.y;
						newPos.z += offsetAll.z;
					Instantiate (cube, newPos, transform.rotation);
//						GameObject block = (GameObject)Instantiate (cube, newPos, transform.rotation);
//						block.transform.parent = transform;
				}

			}
		}
	}
}


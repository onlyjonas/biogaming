﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class KeyScript : MonoBehaviour {
	
	public float myLifeTime = 5.0f;
	private float timeLeft;
	private GameControler gameControler;
	public AudioClip collectSound;

	void Start () {

		GameObject main = GameObject.Find ("Main");
		if (main) gameControler = main.GetComponent<GameControler>();

		StartCoroutine ("LifeTime");
	}
	
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			gameControler.AddKey ();
			AudioSource.PlayClipAtPoint (collectSound, transform.position);
			Destroy (gameObject);
		}
	}

	IEnumerator LifeTime() {
		yield return new WaitForSeconds(myLifeTime);
		Destroy (gameObject);
	}

}
